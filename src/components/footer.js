import React from 'react'
import "../styles/Footer.css"

const footer = () => (
  <div id="Footer">
    Copyright &copy; 2019 Hacker Space NSSCE. All Rights Reserved.
  </div>
  )

export default footer
