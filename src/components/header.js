import { Link } from "gatsby"
import PropTypes from "prop-types"
import "../styles/navbar.css"
import React from "react"
import Logo from './nav-logo'

const Header = ({ siteTitle }) => (
  <div>
    <nav>
     <Logo/>
    <div class="menubar">
      <ul class="menu-wrapper">
        <li class="menu-items"><Link to="/">Home</Link></li>
        <li class="menu-items"><Link to="/about/">About</Link></li>
        <li class="menu-items"><Link to="/gallery/">Gallery</Link></li>
        <li class="menu-items"><Link to="/projects/">Projects</Link></li>
        <li class="menu-items"><Link to="/achievements/">Achievements</Link></li>
      </ul>
      </div>
    </nav>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
