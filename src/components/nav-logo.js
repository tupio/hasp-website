import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';
import Img from 'gatsby-image';
export default () => {
    const data = useStaticQuery(graphql`
        query MyQuery {
            file(relativePath: { eq: "nav-logo.png" }) {
                childImageSharp {
                    fluid {
                        ...GatsbyImageSharpFluid_tracedSVG
                    }
                }
            }
        }
    `);
    return (
        <div className="brand">
            <Link to='/'>
                <Img
                    fluid={data.file.childImageSharp.fluid}
                    alt='HASP Logo'
                    className='brand-image'
                />
            </Link>
        </div>
    );
};
